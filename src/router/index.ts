import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "About",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/projects",
      name: "Projects",
      component: () => import("../views/ProjectsView.vue"),
      children: [
        {
          path: "/one",
          name: "Artrinix One",
          component: () => null,
          beforeEnter: () => {
            window.location.href = "https://one.artrinix.xyz";
          },
        },
        {
          path: "/arcane",
          name: "Arcane",
          component: () => null,
          beforeEnter: () => {
            window.location.href = "https://arcane.artrinix.xyz";
          },
        },
        {
          path: "/cthulhu",
          name: "Cthulhu",
          component: () => null,
          beforeEnter: () => {
            window.location.href = "https://cthulhu.artrinix.xyz";
          },
        },
        {
          path: "/send",
          name: "Send",
          component: () => null,
          beforeEnter: () => {
            window.location.href = "https://send.artrinix.xyz";
          },
        },
        {
          path: "/wisp",
          name: "Wisp",
          component: () => null,
          beforeEnter: () => {
            window.location.href = "https://wisp.artrinix.xyz";
          },
        },
      ],
    },
    {
      path: "/discord",
      name: "Discord",
      component: () => null,
      beforeEnter: () => {
        window.location.href = "https://discord.gg/ft7nQJ57BJ";
      },
    },
    {
      path: "/gitlab",
      name: "GitLab",
      component: () => null,
      beforeEnter: () => {
        window.location.href = "https://gitlab.com/artrinix";
      },
    },
  ],
});

export default router;
