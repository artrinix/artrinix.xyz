/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  separator: "_",
  darkMode: "class",
  theme: {
    FontFace: {
      sans: ["Fira Sans", "Droid Sans", "sans-serif"],
      serif: ["Lato", "Merriweather"],
    },
    extend: {},
  },
  plugins: [require("@tailwindcss/typography"), require("daisyui")],
};
